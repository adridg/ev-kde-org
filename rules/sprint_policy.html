---
title: "KDE Sprint Policy"
layout: page
---

<h2>Preamble</h2>

<p>KDE is a world-wide distributed volunteer-based community. To stimulate
productive high-bandwidth collaboration and community building we hold KDE
sprints, focused meetings of small groups of contributors. The goal of these
sprints is to use face-to-face time to get work done in the most efficient way,
and catalyze longer-term development efforts.</p>

<p>KDE sprints form an essential part of the culture of KDE. They are an
integral
part of the development process, and foster communication, collaboration,
coordination, and forward planning. In recognition of its importance they are
one of the primary activities of KDE e.V. to support the community.</p>

<h2>1. Sprint Format</h2>

<p>Sprints are typically two or three day events, often over a long weekend,
with
around ten participants. Something like fifteen participants is an upper
boundary, sprints with less participants are usually the more productive
ones.</p>

<p>Usually sprints are dedicated events and not part of larger events to provide
focus and the opportunity to spend all energy on the sprint work. The goal is
to provide an environment which allows people to be most productive, achieve
flow, and create results.</p>

<p>Topics to be covered at sprints are coming from the parts of the community,
which need face-to-face time to accelerate development. Sprints are supposed to
increase existing momentum, to strengthen strong teams, and to support
initiatives coming from the community. They are a tool to make good work
great.</p>

<p>To support introduction and integration of new contributors into the
community
sprints are required to include new contributors. At least ten percent of the
participants should be people who joined the community recently, have never
been at a sprint before, or have just started to work with the group doing the
sprint. This is sometimes referred to as "newbie quota".</p>

<h2>2. Organization</h2>

<p>Sprint organization is driven by a person acting as the organizer. This
person is
the main point of contact and responsible for coordination of the participants,
finding a date and a location for the sprint, requesting financial support from
KDE e.V., and making sure that progress and results of the sprint are reported
to KDE e.V., the community, and the public.</p>

<p>KDE e.V., and specifically the board and employees, support
the organizer in all this activities by providing help, giving feedback and
guidance, facilitating communication, or whatever else is needed to successfully
run the sprint.</p>

<p>As a central tool the community wiki is used for planning and collecting more
detailed information, especially about the content of the sprint. The sprint
group may make use of other tools as they see fit.</p>

<p>As location for the sprint we usually use meeting rooms provided by companies
supporting the sprint. Other arrangments are possible. KDE e.V. can help with
finding a suitable location.</p>

<p>For accommodation we usually do a group reservation in an affordable hotel or
hostel. KDE e.V. can help with this.</p>

<p>Travel is organized by participants on their own. If necessary KDE e.V. can
help here as well, e.g. with booking of flights for people who can't afford to
advance the money.</p>

<p>Creativity in organizing sprints is welcome. The goal always should be to
create
a productive atmosphere, which allows participants to spend their time in the
best way to be productive and create valuable results.</p>

<h2>3. Financial support</h2>

<p>KDE e.V. supports sprints financially through the money it gets from
supporting
members and donors. The money is used responsibly in the interest of the KDE
community and its goals to create great free software for a wide range of
users.</p>

<p>The sprint organizer is responsible for requesting financial support from KDE
e.V. for the overall sprint.</p>

<p>Participants are encouraged to seek financial support from other parties,
e.g. from their employers, before requesting support from KDE e.V.</p>

<p>Organizers are encouraged to choose locations which keep travel and
accommodation costs down. Local sprints are preferred, though we try to bring
all people together who are needed to get most out of a sprint, even if
that involves long-distance travel.</p>

<p>Participants advance the costs and are reimbursed by KDE e.V. on sending in
their receipts. Exceptions are possible on request if there is a good
reason. For more details please see the reimbursement policy.</p>

<h3>3.1 Procedure for financial support requests</h3>

<p>Organizers prepare a request for support including the following
information:</p>

<ul>
<li>Goal of the sprint</li>
<li>Number and names of participants, including indication of new community
members</li>
<li>Location of the sprint</li>
<li>Estimated costs which are requested to be reimbursed</li>
<li>Any additional supporting information to help with deciding about the
request</li>
</ul>

<p>Requests are send to the board of KDE e.V. via email at kde-ev-board@kde.org.
The board acknowledges reception of the request and clarifies open question if
needed.</p>

<p>Requests should be sent no later than four weeks before the sprint is taking
place.</p>

<p>To encourage to plan well in advance and to be able to distribute a limited
budget in a fair way, sprints are regularly approved four times a year at the
beginning of each quarter. Deadline for sending requests are January 1st, April
1st, July 1st, and October 1st.</p>

<p>The board approves requests for sprints according to the available budget
for each quarter. Unused budget is available for more short-notice
requests sent in during the quarter. Remaining budget is transferred to the next
quarter. If there is not enough budget to accommodate all requested sprints it's
up to the board's discretion to choose the sprints which promise to be most
valuable.</p>

<p>If any costs change the organizer of the sprint notifies the board and a
decision is made in coordination of the organizer and the board how to deal
with the change. The board might refuse an increase of costs if budget or other
considerations suggest that.</p>

<p>The board might grant exceptions to the procedure, if there are good reasons
for doing so.</p>

<h2>4. Reports</h2>

<p>Reporting about the activities and results of a sprint is essential. It's
needed to inform the community, to provide donors and supporters with
information what happened to their money, and to motivate people to support
future sprints.</p>

<p>The participants of a sprint are required to provide the following
reports:</p>

<ul>
<li>A landing page on community.kde.org with an overview of the sprint and its
results, and links to additional material, e.g. blogs, dot stories,
additional content on the Wiki, press coverage, code written at the sprint,
photos from the sprint, etc., and organizational background such as the link
to the page at sprints.kde.org.</li>
<li>A story on dot.kde.org with a public report about the sprint.</li>
<li>A summary of the sprint for the quarterly reports of KDE e.V.</li>
</ul>

<p>Additional reports, e.g. an internal report for the members of KDE e.V. or
the
board or anything else which is recognized as useful is appreciated as well.</p>

<hr/>

<p><em>This policy is maintained by the board of the KDE e.V.</em></p>
