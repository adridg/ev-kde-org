---
title: 'KDE e.V. is looking for a project/community manager and a project lead/event manager for environmental sustainability project'
date: 2021-04-05 18:00:00 
layout: post
---

> Edit 2021-06-02: applications for these positions **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for two people to run a project related to the environmental sustainability of our software.
The positions we are looking to fill are those of a project lead who can also do some event management duties as well as a project/community manager.
Please see the [job ad for the project lead]({{ '/resources/jobad-projectlead2021.pdf' | prepend: site.url }}) and [job ad for the project/community manager]({{ '/resources/jobad-projectcommunitymanager2021.pdf' | prepend: site.url }}) for more details about this employment opportunity. 
We are looking forward to your application.
