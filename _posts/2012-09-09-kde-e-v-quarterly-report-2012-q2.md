---
title: 'KDE e.V. Quarterly Report 2012 Q2'
date: 2012-09-09 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2012_Q2.pdf">second quarterly report of 2012</a>.

This report covers April through June 2012, including statements from the board, reports from sprints, financial information and a special part about organizing Akademy, our annual conference.
      
