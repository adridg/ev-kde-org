---
title: 'KDE e.V. is looking for a documentation writer'
date: 2021-03-24 20:00:00 
layout: post
---

> Edit 2021-06-02: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is 
looking for a documentation writer to help KDE improve its (online, technical)
documentation. Please see the 
[call for proposals]({{ '/resources/documentationsupport-callforproposals-2021.pdf' | prepend: site.url }})
for more details about this contract opportunity. 
We are looking forward to your application.

> The full call for proposals has more details.

KDE e.V. is looking for a person with technical writer experience to 
update KDE‘s documentation across its
various projects. Documentation for our software and community is 
constantly evolving and requires careful
planning and maintenance, as well as regular communication and close collaboration with our contributor teams.
You will be building on the outcome of previous documentation work 
([1]({{ '/resources/01 KDE Developer Documentation Update Project Report.pdf' | prepend: site.url }}),
[2]({{ '/resources/02 KDE Developer Documentation Report Update - 2019-12-09.pdf' | prepend: site.url }})), that analyzed the documentation
needs of our projects and identified key areas of improvement. These include the KDE API Documentation, our
Community and TechBase Wikis, our Developer Guide, as well as documentation for Plasma (Desktop and
Mobile), Frameworks and Applications. You will be working directly with the existing documentation team of
volunteers and the Board of Directors.
The ideal candidate would be experienced in technical writing and familiar with the process of writing and
maintaining documentation. You will be able to deliver quality content with the goal of helping more contributors
to get involved with our projects and make their first contributions.
