---
title: Reports
layout: page
---

The KDE e.V. collects reports about its activities. Here you can find all
the public [community reports](#kde-ev-community-reports) and [minutes of the AGM](#meetings).

## KDE e.V. Community Reports

<h3>2019</h3>
<ul>
<li><a href="ev-2019/" target="_blank">KDE e.V. Report for 2019 (Issue 36)</a></li>
</ul>

<h3>2018</h3>
<ul>
<li><a href="ev-2018/" target="_blank">KDE e.V. Report for 2018 (Issue 35)</a></li>
</ul>

<h3>2017</h3>
<ul>
<li><a href="ev-2017/" target="_blank">KDE e.V. Report for 2017 (Issue 34)</a></li>
</ul>

<h3>2016</h3>
<ul>
<li><a href="ev-2016/" target="_blank">KDE e.V. Report for 2016 (Issue 33)</a></li>
</ul>

<h3>2015</h3>
<ul>
<li><a href="ev-2015H2/" target="_blank">KDE e.V. Report for 2nd Half of 2015 (Issue 32)</a></li>
<li><a href="ev-2015H1/" target="_blank">KDE e.V. Report for 1st Half of 2015 (Issue 31)</a></li>
</ul>

<h3>2014</h3>
<ul>
<li><a href="ev-quarterly-2014_Q4.pdf">KDE e.V. Quarterly Report 2014Q4 (Issue 30)</a></li>
<li><a href="ev-quarterly-2014_Q3.pdf">KDE e.V. Quarterly Report 2014Q3 (Issue 29)</a></li>
<li><a href="ev-quarterly-2014_Q1Q2.pdf">KDE e.V. Quarterly Report 2014Q1Q2 (Issue 28)</a></li>
</ul>

<h3>2013</h3>
<ul>
<li><a href="ev-quarterly-2013_Q4.pdf">KDE e.V. Quarterly Report 2013Q4 (Issue 27)</a></li>
<li><a href="ev-quarterly-2013_Q2Q3.pdf">KDE e.V. Quarterly Report 2013Q2/Q3 (Issue 26)</a></li>
<li><a href="ev-quarterly-2013_Q1.pdf">KDE e.V. Quarterly Report 2013Q1 (Issue 25)</a></li>
</ul>

<h3>2012</h3>
<ul>
<li><a href="ev-quarterly-2012_Q4.pdf">KDE e.V. Quarterly Report 2012Q4 (Issue 24)</a></li>
<li><a href="ev-quarterly-2012_Q3.pdf">KDE e.V. Quarterly Report 2012Q3 (Issue 23)</a></li>
<li><a href="ev-quarterly-2012_Q2.pdf">KDE e.V. Quarterly Report 2012Q2 (Issue 22)</a></li>
<li><a href="ev-quarterly-2012_Q1.pdf">KDE e.V. Quarterly Report 2012Q1 (Issue 21)</a></li>
</ul>

<h3>2011</h3>
<ul>
<li><a href="ev-quarterly-2011_Q4.pdf">KDE e.V. Quarterly Report 2011Q4 (Issue 20)</a></li>
<li><a href="ev-quarterly-2011_Q3.pdf">KDE e.V. Quarterly Report 2011Q3 (Issue 19)</a></li>
<li><a href="ev-quarterly-2011_Q2.pdf">KDE e.V. Quarterly Report 2011Q2 (Issue 18)</a></li>
<li><a href="ev-quarterly-2011_Q1.pdf">KDE e.V. Quarterly Report 2011Q1 (Issue 17)</a></li>
</ul>

<h3>2010</h3>

<ul>
<li><a href="ev-quarterly-2010Q4.pdf">KDE e.V. Quarterly Report 2010Q4 (Issue 16)</a></li>
<li><a href="ev-quarterly-2010Q3.pdf">KDE e.V. Quarterly Report 2010Q3 (Issue 15)</a></li>
<li><a href="ev-quarterly-2010Q2.pdf">KDE e.V. Quarterly Report 2010Q2 (Issue 14)</a></li>
<li><a href="ev-quarterly-2009Q2-2010Q1.pdf">KDE e.V. Quarterly Report 2009Q2-2010Q1 (Issue 13)</a></li>
</ul>

<h3>2009</h3>

<ul>
<li><a href="ev-quarterly-2009Q1.pdf">KDE e.V. Quarterly Report 2009Q1 (Issue 12)</a></li>
</ul>

<h3>2008</h3>

<ul>
<li><a href="ev-quarterly-2008Q3-Q4.pdf">KDE e.V. Quarterly Report 2008Q3/Q4 (Issue 11)</a></li>
<li><a href="ev-quarterly-2008Q1-Q2.pdf">KDE e.V. Quarterly Report 2008Q1/Q2 (Issue 10)</a></li>
</ul>

<h3>2007</h3>

<ul>
<li><a href="ev-quarterly-2007Q3-Q4.pdf">KDE e.V. Quarterly Report 2007Q3/Q4 (Issue 9)</a></li>
<li><a href="ev-quarterly-2007Q2.pdf">KDE e.V. Quarterly Report 2007Q2 (Issue 8)</a></li>
<li><a href="ev-quarterly-2007Q1.pdf">KDE e.V. Quarterly Report 2007Q1 (Issue 7)</a></li>
</ul>

<h3>2006</h3>

<ul>
<li><a href="ev-quarterly-2006Q4.pdf">KDE e.V. Quarterly Report 2006Q4 (Issue 6)</a></li>
<li><a href="ev-quarterly-2006Q3.pdf">KDE e.V. Quarterly Report 2006Q3 (Issue 5)</a></li>
<li><a href="ev-quarterly-2006Q2.pdf">KDE e.V. Quarterly Report 2006Q2 (Issue 4)</a></li>
<li><a href="ev-quarterly-2006Q1.pdf">KDE e.V. Quarterly Report 2006Q1 (Issue 3)</a></li>
</ul>

<h3>2005</h3>

<ul>
<li><a href="ev-quarterly-2005Q4.pdf">KDE e.V. Quarterly Report 2005Q4 (Issue 2)</a></li>
<li><a href="ev-quarterly-2005Q3.pdf">KDE e.V. Quarterly Report 2005Q3 (Issue 1)</a></li>
</ul>

## Meetings

The KDE e.V. holds a general assembly of its members each year.
On this assembly reports about the activity of the e.V. are given,
elections are held and decisions about matters of the e.V. are taken.

- 2020 AGM minutes ([german](2020-de))
- 2019 AGM minutes ([german PDF](2019-de.pdf))
- 2018 AGM minutes ([german PDF](2018-de.pdf))
- 2017 AGM minutes ([german PDF](2017-de.pdf))([english PDF](2017-en.pdf))
- 2016 AGM minutes ([german PDF](2016-de.pdf))
- 2015 AGM minutes ([german PDF](2015-de.pdf))([english PDF](2015-en.pdf))
- 2014 AGM minutes ([german PDF](2014-de.pdf))([english PDF](2014-en.pdf))
- 2013 AGM minutes ([german PDF](2013-de.pdf))
- 2012 AGM minutes ([german PDF](2012-de.pdf))
- 2011 AGM minutes ([german PDF](2011-de.pdf))([english PDF](2011-en.pdf))
- 2010 AGM minutes ([german PDF](2010-de.pdf))
- 2009 AGM minutes ([german PDF](2009-de.pdf))
- 2008 AGM minutes ([german PDF](2008-de.pdf))
- 2007 AGM minutes ([english](2007))
- 2006 meeting notes ([english](2006))
- 2005 meeting notes ([english](2005)) and working groups discussion ([english](2005-working-groups-discussion))
- 2004 meeting notes ([english](2004))
- 2003 meeting notes ([english](2003))
- 2002 meeting notes ([english](2002))
